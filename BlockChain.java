import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;
import java.util.Scanner;

public class BlockChain {
	private LinkedList<Block> blockChain = new LinkedList<Block>();
	private String courseName;

	public BlockChain(String courseName) {
		this.courseName = new String(courseName);
	}
	
	public String toString() {
		return this.courseName;
	}

	public void printBlockChain() {
		for(Block index: blockChain) System.out.println(index);	
	}

	public boolean verifyChain() {
		boolean check = true;
		ListIterator<Block> temp = blockChain.listIterator();
		
		while(temp.hasNext()) {
			Block current = temp.next();
			
			if(!temp.hasNext()) break;
			if(!temp.next().isEqual(current)) check = false;
			
			temp.previous();
		}
		
		return check;
	}

	public void addBlock(Scanner keyboard) {
		Block newOne = new Block();
		
		if (blockChain.size() == 0) {
			if (newOne.addInfoToBlock(keyboard, 0)) {
				blockChain.add(newOne);
			}
		}else if (newOne.addInfoToBlock(keyboard, blockChain.getLast().getCurrentHash())) {
			blockChain.add(newOne);
		}
	}

	public void addBadBlock(Scanner keyboard) {
		Random random = new Random();
		Block newOne = new Block();
		if (newOne.addInfoToBlock(keyboard, random.nextFloat())) {
			blockChain.add(newOne);
		}
	}
	
	public boolean fixChain() {	
		for(int i=0; i<blockChain.size(); i++) {
			if(blockChain.get(i).getCurrentHash() != blockChain.get(i+1).getPrevHash()) {
				blockChain.get(i+1).updatePreviousHash(blockChain.get(i).getCurrentHash());
			}
		}
		return true;
	}
	
	public boolean isEmpty() {
		if (blockChain.size() == 0) return true;
		else return false;
	}

};