import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Assign3 {
	private static ArrayList<BlockChain> algonquin = new ArrayList<BlockChain>();

	public Assign3() {

	}

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		int input = 0;

		do {
			displayMenu();
			if (keyboard.hasNextInt()) {
				input = keyboard.nextInt();

				switch (input) {
				case 1:
					if (!displayCourses(keyboard))
						System.out.println("...returning to main menu");
					break;
				case 2:
					if (!addCourse(keyboard))
						System.out.println("...returning to main menu");
					break;
				case 3:
					addBlock(keyboard);
					break;
				case 4:
					verifyChain(keyboard);
					break;
				case 5:
					fixChain(keyboard);
					break;
				case 6:
					break;
				default:
					printHeader("Error", "Incorrect value. Please try again.");
				}
			} else
				printHeader("Error", "Incorrect value. Please enter an integer.");
		} while (input != 6);
	}

	public static boolean displayCourses(Scanner in) {
		printHeader("Algonquin Courses");
		try {
			for (BlockChain index : algonquin)
				System.out.println(index);
		} catch (Exception e) {
			printHeader("Error Loop 1", e.toString());
			return false;
		}

		if (algonquin.size()!=0) {
			try {
				for (BlockChain index : algonquin) {
					printHeader("\n"+index.toString());
					if (!index.isEmpty()) index.printBlockChain();
					else System.out.println("Empty\n");
				}
			} catch (Exception e) {
				printHeader("Error Loop 2", e.toString());
				return false;
			}
		} else {
			System.out.println("Empty\n");
		}
		return true;
	}

	public static boolean addCourse(Scanner in) {
		String input;
		BlockChain temp;

		printHeader("Add Course");
		System.out.println("Enter course code to add:");
		input = in.next();

		try {
			temp = new BlockChain(input);
		} catch (Exception e) {
			printHeader("Error Building Block", e.toString());
			return false;
		}

		try {
			algonquin.add(temp);
			return true;
		} catch (Exception e) {
			printHeader("Error Adding to Algonquin", e.toString());
			return false;
		}
	}

	public static boolean addBlock(Scanner in) {
		int count;
		int input = -1;
		char goodBad = 'k';
		
		do {
			System.out.println("choose:\n[g] Add good block\n[b] Add bad block");
			goodBad = in.next().charAt(0);
		}while(goodBad != 'g' && goodBad != 'b');
		
		do {
			count = 0;
			printHeader("Add Block");

			for (BlockChain index : algonquin) {
				System.out.println("[" + count++ + "] " + index.toString());
			}
			
			 System.out.println("choose course to add block to:");
			if (!in.hasNextInt()) continue;
			input = in.nextInt();
			
		} while (input > (count-1) || input < 0);
		
		if (goodBad == 'g')	algonquin.get(input).addBlock(in);
		else algonquin.get(input).addBadBlock(in);

		return true;
	}

	public static boolean verifyChain(Scanner in) {
		printHeader("Verifying Algonquin Courses");
		for (BlockChain index : algonquin) {
			if (index.verifyChain()) System.out.println("Chain for " + index + " is verified.");
			else System.out.println("Chain for " + index + " is not verified.");
		}
		return true;
	}

	public static boolean fixChain(Scanner in) {
		int count;
		int input = -1;
		
		do {
			count = 0;
			printHeader("Add Block");

			for (BlockChain index : algonquin) {
				System.out.println("[" + count++ + "] " + index.toString());
			}
			
			 System.out.println("choose course to fix:");
			if (!in.hasNextInt()) continue;
			input = in.nextInt();
			
		} while (input > (count-1) || input < 0);
		
		algonquin.get(input).fixChain();
		
		return true;
	}

	public static void displayMenu() {
		printHeader("Main Menu");
		System.out.print("[1] Display college courses\n" + "[2] Add new course\n" + "[3] Add a block\n"
				+ "[4] Verify chains\n" + "[5] Fix a chain\n" + "[6] Quit\n" + "-> ");
	}

	public static void printHeader(String header) {
		System.out.printf("\n  %s\n-----------------------------\n", header);
	}

	public static void printHeader(String header, String addOn) {
		System.out.printf("\n  %s\n-----------------------------\n%s\n", header, addOn);
	}

}
